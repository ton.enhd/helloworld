// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

contract SimpleBank{
    mapping(address => uint256) public balances;
    bool entry = false;

    function deposit() public payable{
        balances[msg.sender] += msg.value;
    }
// code จะไม่เหมือนกับใน slide เพราะอาจารย์แทรกเนื้อหาเพิ่มเติม
    function withdraw(uint amount) public{
        require(!entry);  //ป้องกัน re-entrancy
        entry = true;
        require(balances[msg.sender] >= amount); // เช็คว่าเงินที่ balance ต้องมีมากกว่าที่เขาจะถอน
        // วิธีที่ 1 แต่ไม่ค่อยนิยม
        //payable(msg.sender).transfer(amount);

        // วิธีที่ 2 เหมือนคราวที่เเล้ว แนะนำให้ใช้อันนี้  ข้อดีคือมี return ค่ากลับมาว่า True or False  // ต้องมี 2 บรรรทัดคู่กัน !!!
        (bool success,) = msg.sender.call{value: amount}(""); // ส่งเงิน amount ให้กับคนขอถอน  และเงินจากถูกลบไปจาก smart contact ของเรา
        require(success);

        balances[msg.sender] -= amount;
        entry = false;

        
        //  version ต่ำกว่า 0.7 ต้องมีเรื่องนี้ เพื่อแก้ปัญหา underflow overflow
        //for{
        //    unchecked{
        //        balances[msg.sender] -= amount;
        //    }
       // }

    }

    function getBalance() public view returns (uint256){
        return balances[msg.sender];
    }

}


//contract Hacker{
 //   function attack(){
 //       bank.deposit{value: 1 ether}();
 //       bank.withdraw(1 ether);
 //   }
//}
