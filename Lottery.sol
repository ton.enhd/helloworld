// SPDX-License-Identifier: UNLICENSED
pragma solidity ^0.8.17;

contract Lottery{
    address public manager;
    address [] public players;

    constructor(){
        manager = msg.sender;
    }
    function enter() public payable{
        require(msg.value >= 0.001 ether,"please enter more than 0.001 ether");

        players.push(msg.sender);

    }

    function random() private view returns (uint){
        return uint(keccak256(abi.encodePacked(block.difficulty,block.timestamp,players)));
    }

    function pickWinner() public{
        require(msg.sender == manager, "ONLY MANAGER");

        uint index = random()%players.length;
        (bool success,) = players[index].call{value:(address(this).balance)}("");
        require(success, "Transfer failed.");
        players = new address payable[](0);

    }
}
